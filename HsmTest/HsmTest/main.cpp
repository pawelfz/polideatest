#include "HSM\Queue\Queue.h"
#include "HSM\Events\Event.h"
#include "HSM\States\Base\StateHsmBase.h"
#include "HSM\States\Base\Hsm.h"
#include <iostream>

int main()
{
	//Create queue 
	auto queueHandler = QueueCreate(10, sizeof(Event));

	//Create Hierarchical state machine
	Hsm hsm;

	if(hsm.TestTransitionTable())
		std::cout << "HSM  -  Transition Table Test = Succes\n";
	else
	{
		std::cout << "HSM  -  Transition Table Test = Fail!!!\n";
		std::cout << "Press any button to continue\n";

		char dummy;
		std::cin >> dummy;
	}

	Event eventPowerOn(EventID::PowerOnEvent),
		eventPowerOff(EventID::PowerOffEvent), 
		eventPlaybeckStop(EventID::StopPlaybackEvent), 
		eventPlaybeckStart(EventID::StartPlaybackEvent);


	for (size_t i = 0; i < 25; i++)
	{
		eventPowerOn.SetMessage((char*)&i, 1, i, 0);
		eventPowerOff.SetMessage((char*)&i, 1, i, 0);
		eventPlaybeckStart.SetMessage((char*)&i, 1, i, 0);
		eventPlaybeckStop.SetMessage((char*)&i, 1, i, 0);
	}

	while (1)
	{
		//Used in QueueReceive
		Event receivedEvent(EventID::NoEvent);

		std::cout << "\n\t Select action:\n\n";

		std::cout << "1. Add PowerOnEvent to queue\n";
		std::cout << "2. Add PowerOffEvent to queue\n";
		std::cout << "3. Add StopPlaybackEvent to queue\n";
		std::cout << "4. Add StartPlaybackEvent to queue\n\n";
		std::cout << "5. Get an event from queue\n";
		std::cout << "6. Get Queue information\n\n";

		char c;
		std::cin >> c;

		switch (c)
		{
		case '1':
			QueueSend(queueHandler, &eventPowerOn);
			break;
		case '2':
			QueueSend(queueHandler, &eventPowerOff);
			break;
		case '3':
			QueueSend(queueHandler, &eventPlaybeckStop);
			break;
		case '4':
			QueueSend(queueHandler, &eventPlaybeckStart);
			break;
		case '5':
			QueueReceive(queueHandler, &receivedEvent);
	
			if(receivedEvent.GetEventId() != EventID::NoEvent)
				hsm.OnEvent(&receivedEvent);

			break;
		case '6':
			QueueStats();
			break;
		default:
			break;
		}

		std::cout << "\n\t Current state \n"<< hsm.GetCurrentState()->Name()<<"\n\n";
	}
	return 0;
}