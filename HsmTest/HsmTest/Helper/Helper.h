#pragma once

#define PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )

template<typename To>
void GetBufferFromStruct(To *structObject, char *buffer, size_t size)
{
	*(To *)buffer = *structObject;
}

template<typename To>
void GetStructFromBuffer(To *structObject, char *buffer)
{
	*structObject = *((To *)buffer);
}
