#pragma once

enum StateID;

class IHsm
{
public:
	virtual void OnEnter() = 0;
	virtual void OnExit() = 0;
	virtual StateID GetStateId() = 0;
	//For test
	virtual char* Name() = 0;
};
