#pragma once

#define MAX_NUMBER_OF_QUEUE		5
#define QUEUE_MESSAGE_SIZE		1024

#define QUEUE_OVERSIZE_ERROR	(-1)

typedef int QueueHandle_t;

QueueHandle_t QueueCreate(const size_t queueLength, const size_t itemSize);
void QueueSend(QueueHandle_t queue, const void* msg);
void QueueReceive(QueueHandle_t queue, void* msg);
void QueueStats();


class Queue
{
public:
	void Enqueue(const void* msg);
	void Dequeue(void* msg);

	size_t GetItemCount() { return itemCount; }
	void SetItemCount(size_t newItemCount) { itemCount = newItemCount; }

	size_t GetItemSize() { return itemSize; }
	void SetItemSize(size_t newItemSize) { itemSize = newItemSize; }

	size_t GetSize() { return size; }
	void SetSize() { size = itemCount * itemSize; }

	size_t GetCount();
	size_t GetFreeSpace();

	char* startAddress;
private:
	size_t itemCount;
	size_t itemSize;
	size_t size;
	size_t head;
	size_t tail;
	bool overlap;
};

class QueueManager
{
public:
	QueueManager() {};
	~QueueManager() {};

	void Add() { NumberOfQueue++; };
	size_t GetCount() { return NumberOfQueue; }
	size_t GetSizeByIndex(QueueHandle_t index);
	Queue& GetQueueByIndex(QueueHandle_t index) { return Queues[index]; }
	size_t GetFreeSpace();
private:
	char NumberOfQueue = 0;
	Queue Queues[MAX_NUMBER_OF_QUEUE];

};



