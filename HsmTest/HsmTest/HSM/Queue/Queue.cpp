#include "Queue.h"
#include "../Events/Event.h"
#include <iostream>

char MessageStorage[QUEUE_MESSAGE_SIZE];

QueueManager QUEUE;

QueueHandle_t QueueCreate(const size_t queueLength, const size_t itemSize)
{
	//Check if we can create another queue
	if (QUEUE.GetCount() < MAX_NUMBER_OF_QUEUE)
	{
		//Check if we can fit request
		if (QUEUE.GetFreeSpace() >= (queueLength * itemSize))
		{
			//Fill with new data
			QUEUE.GetQueueByIndex(QUEUE.GetCount()).SetItemCount(queueLength);
			QUEUE.GetQueueByIndex(QUEUE.GetCount()).SetItemSize(itemSize);
			QUEUE.GetQueueByIndex(QUEUE.GetCount()).SetSize();
			QUEUE.GetQueueByIndex(QUEUE.GetCount()).startAddress = &MessageStorage[QUEUE_MESSAGE_SIZE - QUEUE.GetFreeSpace()];
			
			//Add new queue
			QUEUE.Add();

			std::cout << "Created queue with " << (queueLength * itemSize) << " bytes length!" << " Free available space = " << QUEUE.GetFreeSpace() << " bytes\n";

			//Return handler
			return QUEUE.GetCount();
		}
		else
		{
			std::cout << "You do not have " << (queueLength * itemSize) << " bytes space to create new queue!" << " Free available space = " << QUEUE.GetFreeSpace()<<" bytes\n";

			return QUEUE_OVERSIZE_ERROR;
		}
	}
	else
	{
		std::cout << "You have already created " << MAX_NUMBER_OF_QUEUE << " queueus\n";

		return QUEUE_OVERSIZE_ERROR;
	}
}

void QueueSend(QueueHandle_t queue, const void* msg)
{
	if (QUEUE.GetCount() >= queue)
	{
		QUEUE.GetQueueByIndex(queue-1).Enqueue(msg);
	}
	else
		std::cout << "QueueSend - Queue index out of range! You have "<< QUEUE.GetCount() << " queue(s)\n";
}

void QueueReceive(QueueHandle_t queue, void* msg)
{
	if (QUEUE.GetCount() >= queue)
	{
		QUEUE.GetQueueByIndex(queue-1).Dequeue(msg);
	}
	else
		std::cout << "QueueReceive - Queue index out of range! You have " << QUEUE.GetCount() << " queue(s)\n";
}

void QueueStats()
{
	std::cout << "\nFree space - " << QUEUE.GetFreeSpace() << "\n";
	std::cout << "Number of queues - " << QUEUE.GetCount() << "\n";

	for (size_t i = 0; i < QUEUE.GetCount(); i++)
	{
		std::cout << "\tQueue "<< i+1 << "\n";

		std::cout << "\t\t Size " << QUEUE.GetQueueByIndex(i).GetSize() << "\n";
		std::cout << "\t\t Item size " << QUEUE.GetQueueByIndex(i).GetItemSize()<< "\n";
		std::cout << "\t\t Item spot " << QUEUE.GetQueueByIndex(i).GetItemCount() << "\n";

		std::cout << "\t\t Free space " << QUEUE.GetQueueByIndex(i).GetFreeSpace() << "\n";
		std::cout << "\t\t Items in queue " << QUEUE.GetQueueByIndex(i).GetCount() << "\n";
	}
}

size_t QueueManager::GetFreeSpace()
{
	size_t freeSpace = QUEUE_MESSAGE_SIZE;

	if (QUEUE.GetCount() > 0)
	{
		for (size_t i = 0; i < QUEUE.GetCount(); i++)
		{
			freeSpace -= QUEUE.GetQueueByIndex(i).GetSize();
		}
	}

	return freeSpace;
}

size_t QueueManager::GetSizeByIndex(QueueHandle_t index)
{
	if (QUEUE.GetCount() >= index)
		return QUEUE.GetQueueByIndex(index).GetSize();
}

void Queue::Enqueue(const void * msg)
{
	if (GetCount() >= itemCount)
	{
		std::cout << "Queue is already full!!!\n";

		return;
	}

	Event * eventMessage = (Event*)msg;

	if(eventMessage->GetSize() > itemSize)
	{
		std::cout << "Enqueue fail!!! Your message is to long \n";
		return;
	}

	eventMessage->Deserialize(&startAddress[head * itemSize]);
	
	head++;

	if (head >= itemCount)
	{
		if (GetCount() <= itemCount)
		{
			head = 0;
			overlap = true;
		}
	}

	std::cout << "Enqueue success!!! You can add "<< itemCount - GetCount() <<" item(s) to this queue\n";
}

void Queue::Dequeue(void * msg)
{
	if (GetCount() == 0)
	{
		head = tail = 0;
		std::cout << "Queue is empty!!!\n";
		return ;
	}

	Event  eventMessage ;

	GetStructFromBuffer((Event*)msg, &startAddress[tail * itemSize]);

	tail++; 

	if (tail >= itemCount)
	{
		tail = 0;
		overlap = false;
	}

	msg = (void*)&eventMessage;
}

size_t Queue::GetCount()
{
	if (head > tail) return head - tail;
	else if(head < tail) return itemCount - tail + head;
	else if (head == tail)
	{
		if (overlap)
			return itemCount;
		else
			return 0;
	}
}

size_t Queue::GetFreeSpace()
{
	return (itemCount - GetCount()) * itemSize;
}
