#pragma once
#include "../../../StateOn/StateOn.h"

class Hsm;

class StatePlaybackStopped :public StateOn
{
public:
	StatePlaybackStopped();
	~StatePlaybackStopped();

private:
	void PlaybackStart(Hsm &stateMachine, Event *e, StateHsmBase *newState);

	// Inherited via IHsm
	virtual void OnEnter() override;
	virtual void OnExit() override;
	virtual StateID GetStateId() override;
	virtual char*  Name() override;
};

