#include "StatePlaybackStopped.h"
#include <iostream>
#include "../../../Base/Hsm.h"

StatePlaybackStopped::StatePlaybackStopped()
{
}


StatePlaybackStopped::~StatePlaybackStopped()
{
}

void StatePlaybackStopped::PlaybackStart(Hsm & stateMachine, Event * e, StateHsmBase *newState)
{
	std::cout << "Playback Start from Playback Stopped State\n";
	
	//Copy data from event to State container
	char temp[sizeof(Event)];
	e->Deserialize(temp);
	stateMachine.currentState->lastEventData.Serialize(temp);
}

//Action fire on entering to this state
void StatePlaybackStopped::OnEnter()
{
	std::cout << "Entering to State Playback Stopped\n";
}

//Action fire on exiting from this state
void StatePlaybackStopped::OnExit()
{
	lastEventData;
	std::cout << "Exiting from State Playback Stopped\n";
}

char * StatePlaybackStopped::Name()
{
	return (char*)"StatePlaybackStopped\0";
}

StateID StatePlaybackStopped::GetStateId()
{
	return StateID::StatePlaybackStoppedId;
}

