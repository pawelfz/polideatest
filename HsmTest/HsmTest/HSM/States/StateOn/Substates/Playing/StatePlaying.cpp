#include "StatePlaying.h"
#include <iostream>
#include "../../../Base/Hsm.h"

//Contructor
StatePlaying::StatePlaying()
{
}

//Destructor
StatePlaying::~StatePlaying()
{
}

void StatePlaying::PlaybackStop(Hsm & stateMachine, Event * e, StateHsmBase *newState)
{
	std::cout << "Playback Stop from Playing State\n";
}

//Action fire on entering to this state
void StatePlaying::OnEnter()
{
	//We can use data received in StatePlaybackStopped
	lastEventData;
	std::cout << "Entering to State Playing\n";
}

//Action fire on exiting from this state
void StatePlaying::OnExit()
{
	std::cout << "Exiting from State Playing\n";
}

char * StatePlaying::Name()
{
	return (char*)"StatePlaying\0";
}

StateID StatePlaying::GetStateId()
{
	return StateID::StatePlayingId;
}

