#pragma once
#include "../../../StateOn/StateOn.h"

class Hsm;

class StatePlaying: public StateOn
{
public:
	StatePlaying();
	~StatePlaying();

private:
	void PlaybackStop(Hsm &stateMachine, Event *e, StateHsmBase *newState);

	// Inherited via IHsm
	virtual void OnEnter() override;
	virtual void OnExit() override;
	virtual StateID GetStateId() override;
	virtual char*  Name() override;
};

