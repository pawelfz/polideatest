#pragma once
#include "../Base/StateHsmBase.h"
#include "../../Interfaces/IHsm.h"

class Hsm;

class StateOn: public StateHsmBase, public IHsm
{
public:
	StateOn();
	~StateOn();

private:
	void PowerOff(Hsm &stateMachine, Event *e, StateHsmBase *newState);

	// Inherited via IHsm
	virtual void OnEnter() override;
	virtual void OnExit() override;
	virtual StateID GetStateId() override;
	virtual char * Name() override;
};

