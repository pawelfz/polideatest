#include "StateOn.h"
#include <iostream>
#include "../Base/Hsm.h"
#include "../../Queue/Queue.h"

StateOn::StateOn()
{
}

StateOn::~StateOn()
{
}

//Action fire on PowerEvent in StateOn
void StateOn::PowerOff(Hsm &stateMachine, Event *e, StateHsmBase *newState)
{
	std::cout << "Power off from State On\n";
}

//Action fire on entering to StateOn state
void StateOn::OnEnter()
{
	std::cout << "Entering to State On\n";
}

//Action fire on exiting from StateOn state
void StateOn::OnExit()
{
	std::cout << "Exiting from State on\n";
}

char * StateOn::Name()
{
	return (char*)"StateOn\0";
}

StateID StateOn::GetStateId()
{
	return StateID::StateOnId;
}
