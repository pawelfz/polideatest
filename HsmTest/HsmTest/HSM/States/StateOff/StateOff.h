#pragma once
#include "../Base/StateHsmBase.h"
#include "../../Interfaces/IHsm.h"
#include "../../Events/Event.h"

class Hsm;

class StateOff : public StateHsmBase, public IHsm
{
public:
	StateOff();
	~StateOff();
	void PowerOn(Hsm &stateMachine, Event *e,StateHsmBase *newState);
private:

	// Inherited via IHsm
	virtual void OnEnter() override;
	virtual void OnExit() override;
	virtual StateID GetStateId() override;

	virtual char * Name() override;
};

