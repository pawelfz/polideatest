#include "StateOff.h"
#include <iostream>
#include "../Base/Hsm.h"

//Action fire on PowerEvent on this state
void StateOff::PowerOn(Hsm &stateMachine, Event *e, StateHsmBase *newState)
{
	std::cout << "Power on from State Off\n";
}

//Constuctor
StateOff::StateOff()
{
}

//Desctructor
StateOff::~StateOff()
{
}

//Action fire on entering to this state
void StateOff::OnEnter()
{
	std::cout << "Entering to State Off\n";
}

//Action fire on exiting from this state
void StateOff::OnExit()
{
	std::cout << "Exiting from State Off\n";
}

char * StateOff::Name()
{
	return (char*)"StateOff\0";
}

StateID StateOff::GetStateId()
{
	return StateID::StateOffId;
}
