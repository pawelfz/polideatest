#pragma once

#include "StateHsmBase.h"
#include "../StateOff/StateOff.h"
#include "../StateOn/StateOn.h"
#include "../StateOn/Substates/PlaybackStopped/StatePlaybackStopped.h"
#include "../StateOn/Substates/Playing/StatePlaying.h"
#include "../../Events/Event.h"

#define TRANSITION_NUMBER	5

class Hsm;

struct Transition {
	StateHsmBase* currentState;
	StateHsmBase* nextState;
	EventID triggerEventId;
} ;

class Hsm
{
public:
	Hsm();
	~Hsm();
	bool TestTransitionTable();
	void OnEvent(Event * newEvent);
	void SetNewState(StateHsmBase *newState) { lastState = currentState; currentState = newState; }
	StateHsmBase* GetCurrentState() {return currentState; }

private:

	Transition transition[TRANSITION_NUMBER] =
	{
		{&stateOff,					&statePlaybackStopped,		EventID::PowerOnEvent},
		{&statePlaybackStopped,		&stateOff,					EventID::PowerOffEvent},
		{&statePlaybackStopped,		&statePlaying,				EventID::StartPlaybackEvent},
		{&statePlaying,				&statePlaybackStopped,		EventID::StopPlaybackEvent},
		{&statePlaying,				&stateOff,					EventID::PowerOffEvent},
	};

	StateOff stateOff;
	StateOn stateOn;
	StatePlaybackStopped statePlaybackStopped;
	StatePlaying statePlaying;

	StateHsmBase* currentState;
	StateHsmBase* lastState;
	
	friend StatePlaybackStopped;
};

