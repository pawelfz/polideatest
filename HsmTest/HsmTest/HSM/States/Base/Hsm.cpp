#include "Hsm.h"

Hsm::Hsm()
{
	currentState = &stateOff;
}

Hsm::~Hsm()
{
}

bool Hsm::TestTransitionTable()
{
	for (size_t pairNumber = 0; pairNumber < TRANSITION_NUMBER; pairNumber++)
	{
		//Get pair of currentState and triggereventId for compare with other transition 
		StateID currentStateId = transition[pairNumber].currentState->GetStateId();
		EventID currentEventId = transition[pairNumber].triggerEventId;
		
		StateID nextStateRepetition[TRANSITION_NUMBER] = { StateID::StateInvalidId };
		nextStateRepetition[0] = transition[pairNumber].nextState->GetStateId();

		int nextStateRepetitionIndex = 1;

		for (size_t i = 0; i < TRANSITION_NUMBER; i++)
		{
			if (i == pairNumber) continue;

			if (transition[i].currentState->GetStateId() == currentStateId && transition[i].triggerEventId == currentEventId)
			{
				nextStateRepetition[nextStateRepetitionIndex++] = transition[i].nextState->GetStateId();

				//Check repetition
				for (size_t j = 0; j < nextStateRepetitionIndex; j++)
				{
					//If The same currentState and triggerEvent has diffrent nextState 
					if (nextStateRepetition[j] != transition[i].nextState->GetStateId())
						return false;
				}
			}
		}
	}

	return true;
}

void Hsm::OnEvent(Event * newEvent)
{
	for (size_t i = 0; i < TRANSITION_NUMBER; i++)
	{
		if (transition[i].triggerEventId == newEvent->GetEventId())
		{
			if (transition[i].currentState->GetStateId() == currentState->GetStateId())
			{
				switch (newEvent->GetEventId())
				{
					case EventID::PowerOffEvent:
						currentState->PowerOff(*this, newEvent, transition[i].nextState);
						break;
					case EventID::PowerOnEvent:
						currentState->PowerOn(*this, newEvent, transition[i].nextState);
						break;
					case EventID::StartPlaybackEvent:
						currentState->PlaybackStart(*this, newEvent, transition[i].nextState);
						break;
					case EventID::StopPlaybackEvent:
						currentState->PlaybackStop(*this, newEvent, transition[i].nextState);
						break;
				}

				SetNewState(transition[i].nextState);

				lastState->OnExit();

				currentState->OnEnter();

				break;
			}

		}
	}
}
