#pragma once
#include "../../Events/Event.h"

 class Hsm;

 enum StateID
 {
	 StateInvalidId = 0,
	 StateOffId,
	 StateOnId,
	 StatePlaybackStoppedId,
	 StatePlayingId,
 };

 class StateHsmBase
{
public:
	StateHsmBase() {};
	~StateHsmBase() {};

	virtual void PowerOn(Hsm &stateMachine, Event *e, StateHsmBase *newState) {}
	virtual void PowerOff(Hsm &stateMachine, Event *e, StateHsmBase *newState) {}
	virtual void PlaybackStop(Hsm &stateMachine, Event *e, StateHsmBase *newState) {}
	virtual void PlaybackStart(Hsm &stateMachine, Event *e, StateHsmBase *newState) {}

	virtual void OnEnter() {}
	virtual void OnExit() {}
	virtual StateID GetStateId() { return StateID::StateInvalidId; }

	virtual char* Name() { return nullptr; }
	

	Event lastEventData;
};

