#include "Event.h"
#include <iostream>

Event::Event()
{
	eventId = EventID::NoEvent;
	Clear();
}

//Set length long bytes from sourceBuffer into [message] with sourceOffset and/or destinationOffset. Return negative value if fail
size_t Event::SetMessage(char * sourceBuffer, size_t length, size_t destinationOffset, size_t sourceOffset)
{
	if (length <= 0 || destinationOffset < 0 || sourceOffset < 0) return MESSAGE_OUT_OF_RANGE;
	if (destinationOffset + length > EVENT_MESSAGE_LENGTH) return MESSAGE_OUT_OF_RANGE;

	//Update message
	memcpy(&message[destinationOffset], &sourceBuffer[sourceOffset], length);
	
	//Compute messageLength
	if (destinationOffset < messageLength)
		messageLength += (length - destinationOffset);
	else
		messageLength = destinationOffset + length;

	return 0;
}

//Get message from event. Copy to destination buffer [numberOfBytes] bytes from [message] with [offset]
size_t Event::GetMessage(char* destintaionBuffer, size_t numberOfBytes, size_t offset)
{
	if(numberOfBytes <= 0 || offset < 0 )return MESSAGE_OUT_OF_RANGE;
	if (numberOfBytes + offset > messageLength) return MESSAGE_OUT_OF_RANGE;

	//Copy message
	memcpy(destintaionBuffer, &message[offset], numberOfBytes);

	return numberOfBytes;
}


//Print basic properties of Event object
void Event::Print()
{
	std::cout << "EventId -" << eventId << "\n";
	std::cout << "Message Length -" << messageLength << "\n";
}

void Event::Clear()
{
	messageLength = 0;
	memset(message, 0, EVENT_MESSAGE_LENGTH);
}
