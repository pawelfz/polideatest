#pragma once

#include "string.h"
#include "../../Helper/Helper.h"


#define EVENT_MESSAGE_LENGTH 32
#define MESSAGE_OUT_OF_RANGE (-1)

enum EventID
{
	NoEvent,
	PowerOnEvent,
	PowerOffEvent,
	StartPlaybackEvent,
	StopPlaybackEvent,
};

PACK(
class Event
{
public:

	Event();
	Event(EventID newEventId) :eventId(newEventId) { Clear(); };
	~Event() {};

	EventID GetEventId() { return eventId; }
	void SetEventId(EventID newEventId) { eventId = newEventId; }
	size_t GetMessageLength() {return messageLength;}

	size_t SetMessage(char * sourceBuffer, size_t length, size_t destinationOffset = 0, size_t sourceOffset = 0);
	size_t GetMessage(char* destintaionBuffer, size_t numberOfBytes, size_t offset = 0);

	char* GetMessageRaw() { return message; }

	void Print();

	size_t GetSize() { return sizeof(Event); }

	void Deserialize(char *destinationAddress) { GetBufferFromStruct(this, destinationAddress, GetSize()); }
	void Serialize(char *sourceAddress) { GetStructFromBuffer(this, sourceAddress); }
private:

	EventID eventId;
	char	message[EVENT_MESSAGE_LENGTH];
	size_t	messageLength;

	void Clear();

});
