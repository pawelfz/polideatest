Projekt został napisany w środowisku Microsoft Visual Studio 2017 Community version.
Jeżeli nie ma możliwości uruchomienia projektu w tym środowisku to wszystkie pliki źródłowe znajdują się w:
 - HsmTest/HsmTest/main.cpp
 - HsmTest/HsmTest/HSM
 - HsmTest/HsmTest/Helper
 

Zarządzanie tablicą przejść: 
W pliku HSM.h 
 - zwiększamy o 1 TRANSITION_NUMBER
 - dodajemy kolejną konfiguracje do tablicy transition.

Dodawanie nowego eventu:
W pliku Event.h
 - dodajemy nowe pole w EventID (enum)
W pliku StateHsmBase.h
 - Dodajemy funkcje wirtualną virtual void yourAction(Hsm &stateMachine, Event *e, StateHsmBase *newState) {}
Jeżeli jest to konieczne, to w pliku z wybranym stanem systemu (np. StateOff.h i StateOff.cpp) deklarujemy i defniujemy obsługę dla stworzonej wyżej funkcji.
W pliku Hsm.cpp 
 - Dodajemy case z nowym eventem do pętli switch. 

Dodawanie nowego stanu:
Tworzymy nową klasę w projekcie cpp,h
 - Tworzymy klasę dziedziczącą po klasie StateHsmBase oraz implementującą interfejs IHsm
 - Implementujemy obsługę funkcji z interfejsu IHsm
 - Implementujemy obsługię wybranych funkcji z klasy StateHsmBase. 
 
 UWAGA! Należy zwrócić szczególną uwagę na funkcje: 
 
 	virtual StateID GetStateId() override; // Unikalny id
	
	virtual char * Name() override; // Unikalny ciąg znaków
	
W pliku StateHsmBase:
 - dodajemy nowe pole w StateID (enum)
W pliku Hsm.h:
Tworzymy obiekt nowej klasy (stanu systemu) w klasie Hsm. 